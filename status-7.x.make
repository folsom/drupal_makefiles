; $Id$
;
; UO Base Drupal 7 Makefile
; ----------------
; This is the base UO Drupal 7 makefile for creating platforms.


; Core version
; ------------

core = 7.x

; API version
; ------------
; Every makefile needs to declare it's Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.

; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = 7.43

; Projects
; --------

; Editing and WYSIWYG
projects[ctools] = 1.9
projects[diff] = 3.2
projects[imagecache_actions] = 1.7
projects[image_resize_filter] = 1.16
projects[imagecrop] = 1.0-rc3
projects[insert] = 1.3
projects[linkit] = 3.5
projects[menu_block] = 2.7
projects[oembed] = 0.1-beta3
projects[wysiwyg] = 2.2

; CCK
projects[email] = 1.3
projects[filefield_sources] = 1.10
projects[phone] = 2.x-dev


; Misc
projects[advanced_help] = 1.3
projects[auto_nodetitle] = 1.0
projects[backup_migrate] = 3.0
projects[date] = 2.9
projects[ds] = 2.7
projects[entity] = 1.7
projects[entityreference] = 1.1
projects[field_group] = 1.4
projects[jquery_update] = 2.7
projects[google_analytics] = 2.2
projects[libraries] = 2.2
projects[lightbox2] = 1.0-beta1
projects[link] = 1.4
projects[linkchecker] = 1.2
projects[module_filter] = 2.0
projects[nagios] = 1.3
projects[nodequeue] = 2.0
projects[pathologic] = 2.12
projects[pathauto] = 1.3
projects[print] = 1.3
projects[redirect] = 1.0-rc3
projects[rules] = 2.9
projects[search404] = 1.3
projects[shib_auth] = 4.3
projects[site_verify] = 1.1
projects[stringoverrides] = 1.8
projects[token] = 1.6
projects[transliteration] = 3.2
projects[uif] = 1.5
projects[views] = 3.13
projects[views_bulk_operations] = 3.2
projects[views_autorefresh] = 1.0
projects[webform] = 3.23

;jquery cycle - REQUIRED for views_slideshow
libraries[jquery.cycle][type] = "libraries"
libraries[jquery.cycle][download][type] = "file"
libraries[jquery.cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"

;json2 - REQUIRED for views_slideshow
libraries[json2][type] = "libraries"
libraries[json2][download][type] = "file"
libraries[json2][download][url] = "https://raw.githubusercontent.com/douglascrockford/JSON-js/master/json2.js"

;our forked views_slideshow to fix recursive makefiles
libraries[views_slideshow][download][type] = "git"
libraries[views_slideshow][download][url] = "ssh://git@git.uoregon.edu/asdm/views_slideshow-nomake.git"
libraries[views_slideshow][download][tag] = 7.x-3.1
libraries[views_slideshow][destination]= "modules/"

; if we enable content access we need to verify the requirement for forcing "private" filesystem and the ramifications
projects[content_access] = 1.2-beta2

;Theming
projects[context] = 3.6
projects[delta] = 3.0-beta11
projects[omega] = 4.3
projects[omega_tools] = 3.0-rc4
projects[zen] = 5.6


; CKEditor
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; Download cycle
; - This is a required library for the views_slideshow module
libraries[jquery.cycle][download][type]= "git"
libraries[jquery.cycle][download][url] = "git://github.com/malsup/cycle.git"
libraries[jquery.cycle][download][tag] = "3.0.3-a"
libraries[jquery.cycle][destination] = "libraries"

; Download libphonenumber-for-PHP
; - This is a required library for the phone module
; libraries[libphonenumber-for-php][download][type] = "git"
; libraries[libphonenumber-for-php][download][url] = "git://github.com/davideme/libphonenumber-for-PHP.git"
; libraries[libphonenumber-for-php][download][branch] = "master"

libraries[libphonenumber-for-php][download][type] = "git"
libraries[libphonenumber-for-php][download][url] = "git://github.com/davideme/libphonenumber-for-PHP.git"
libraries[libphonenumber-for-php][download][revision] = "59f6745ab1b42ef21f9b9d63c9c1c1e86391bcfc"


; ********************************************************
;
; The following UO custom modules are commented out until we can put them on the IT site for distribution
;    - We could pull directly from Git but pushing releases to the IT site feels more stable and like d.o (still
;      up for debate though)
;
; *******************************************************

; Download UO custom modules from Git
; UO Banner module v2
; - This module implements the older version of the UO banner, kept for compatibility 
projects[uobanner][download][type] = "git"
projects[uobanner][download][url] = "https://git.uoregon.edu/scm/uo_drpl_prod/uobanner.git"
projects[uobanner][type] = "module"
projects[uobanner][subdir] = "custom"
projects[uobanner][download][tag] = "7.x-2.4"

; UO Banner module v3
; - This module implements the standard UO header and footer post-2014 rebrand.
projects[uobannerandfooter][download][type] = "git"
projects[uobannerandfooter][download][url] = "https://git.uoregon.edu/scm/uo_drpl_prod/uobanner.git"
projects[uobannerandfooter][type] = "module"
projects[uobannerandfooter][subdir] = "custom"
projects[uobannerandfooter][download][tag] = "7.x-3.5"

; UO Shib Dynamic Roles module
; - Fixes issue with d.o shib_roles module where non-sticky roles are only kept in memory.  Not all 3rd party modules
; - respect the in memory role assignments and actually go to the db insted.  This causes inconsistencies.
; - Unfortunately the default behavior when setting roles to sticky is the roles are never removed when the IdM
; - system removes the user from the role.  This module deals with that by allowing you to set the roles as 'sticky'
; - but flushes them and resets all roles during login.
projects[shib_roles][download][type] = "get"
projects[shib_roles][download][url] = "https://it.uoregon.edu/system/files/software/3540/shibboleth-dynamic-roles-7.x-1.1.tar.gz"
projects[shib_roles][type] = "module"
projects[shib_roles][subdir] = "custom"

; Status Module
projects[service_status][type] = "module"
projects[service_status][subdir] = "custom"
projects[service_status][download][type] = "git"
projects[service_status][download][url] = "ssh://git@git.uoregon.edu/acs/service_status.git"
projects[service_status][download][tag] = "7.x-1.1"

; UO Cosmic Theme
projects[cosmic][download][type] = "git"
projects[cosmic][download][url] = "https://git.uoregon.edu/scm/uo_drpl_prod/uo-cosmic-theme.git"
projects[cosmic][type] = "theme"
projects[cosmic][download][tag] = "7.x-3.0-beta7"

; UO Vanilla Install Profile
projects[uovanilla][type] = "profile"
projects[uovanilla][download][type] = "git"
projects[uovanilla][download][url] = "ssh://git@git.uoregon.edu/AH/uovanilla_profile.git"
projects[uovanilla][download][tag] = "7.x-1.1"


; Enable the following modules by default
dependencies[] = views
dependencies[] = uo_load_balance
dependencies[] = uobanner
dependencies[] = shib_roles
